import { AmazonLoginPage } from './app.po';

describe('amazon-login App', function() {
  let page: AmazonLoginPage;

  beforeEach(() => {
    page = new AmazonLoginPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
