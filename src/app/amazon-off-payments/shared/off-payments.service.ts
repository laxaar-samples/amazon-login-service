import { Injectable,ElementRef } from '@angular/core';
import {StyleConfig} from './style-config.model';
declare var System: any;
declare var require: any;
declare var OffAmazonPayments: any;

@Injectable()
export class OffPaymentsService {
  private sdkURI: string = "https://static-na.payments-amazon.com/OffAmazonPayments/us/js/Widgets.js?sellerId=";
  private sandboxSdkURI: string = "https://static-na.payments-amazon.com/OffAmazonPayments/us/sandbox/js/Widgets.js?sellerId=";
  private sdkLoaded: boolean = false;
  private offAmazonPayments: any;
  private sellerId: string;
  constructor() {

  }
  loadSdk(sellerId: string,sandboxMode:boolean, successCallback?: Function, failureCallback?: Function) {

    if (!sellerId || sellerId == '') {
      return;
    } else {
      this.sellerId = sellerId;
    }
    let sdkAlreadyLoaded = typeof OffAmazonPayments != 'undefined';
    /*If System JS is present, it will be used else for webpack we expect it to be loaded via webpack.config.js*/
    if (typeof System != 'undefined' && !sdkAlreadyLoaded) {
      let uri = (sandboxMode?this.sandboxSdkURI:this.sdkURI) + this.sellerId;
      console.log("SDK URI - "+uri);
      System.config({
        meta: {
          'off-payments': { format: 'global', scriptLoad: true, exports: "OffAmazonPayments" }
        },
        map: {
          'off-payments': uri
        }
      });
      console.log("System JS Detected");
      /*Try to Load Module via System JS*/
      System.import('off-payments').then(refToLoadedModule => {
        console.log("SDK Loaded via System.js");
        console.log(refToLoadedModule);
        this.offAmazonPayments = refToLoadedModule;
        this.afterLoad();
        successCallback ? successCallback() : null;
      }, (error: any) => {
        console.log("Error Occured while loading Amazon SDK")
        failureCallback ? failureCallback(error) : null;
      });

    } else {
      console.log("System JS not found.Checking if sdk has been loaded already");
      if (sdkAlreadyLoaded) {
        console.log("Amazon SDK already Loaded externally")
        this.offAmazonPayments = OffAmazonPayments;
        this.afterLoad();
        successCallback ? successCallback() : null;
      } else {
        console.log("Error Occured while loading Amazon SDK")
        failureCallback ? failureCallback("SDK Not found") : null;
      }
    }
  }
  isSdkLoaded() {
    return this.sdkLoaded;
  }
  afterLoad() {
    this.sdkLoaded = true;

  }
  loadWalletWidget(element:ElementRef,amazonBillingAgreementId:string,styleConfig?:StyleConfig,onReadyCallback?:Function,onPaymentSelectCallback?:Function,onErrorCallback?:Function){
    if(this.sdkLoaded){
      let config:any = {
        sellerId: this.sellerId,
        agreementType: 'BillingAgreement',
        onReady: function(billingAgreement) {
            onReadyCallback?onReadyCallback(billingAgreement):null;
        },
        design: styleConfig.responsive ? { designMode: 'responsive' } : {
  size: { width: styleConfig.width, height: styleConfig.height }
  },
        onPaymentSelect: function(orderReference) {
          // Display your custom complete purchase button
          onPaymentSelectCallback?onPaymentSelectCallback(orderReference):null;
        },
        onError: function(error) {
          // Write your custom error handling
          onErrorCallback?onErrorCallback(error):null;
        }
      };
      if(styleConfig && styleConfig.displayMode && styleConfig.displayMode!=''){
        config.displayMode = styleConfig.displayMode;
      }
      if(amazonBillingAgreementId){
        config.amazonBillingAgreementId = amazonBillingAgreementId;
      }
      new this.offAmazonPayments.Widgets.Wallet(config).bind(element.nativeElement.id);
    }
  }
  loadAddressWidget(element:ElementRef,styleConfig?:StyleConfig,onReadyCallback?:Function,onAddressSelectCallback?:Function,onErrorCallback?:Function){
    if(this.sdkLoaded){
      let config:any = {
        sellerId: this.sellerId,
        onReady: function(orderReference) {
            onReadyCallback?onReadyCallback(orderReference):null;
        },
        design: styleConfig.responsive ? { designMode: 'responsive' } : {
  size: { width: styleConfig.width, height: styleConfig.height }
},
        onAddressSelect: function(orderReference) {
          // Display your custom complete purchase button
          onAddressSelectCallback?onAddressSelectCallback(orderReference):null;
        },
        onError: function(error) {
          // Write your custom error handling
          onErrorCallback?onErrorCallback(error):null;
        }
      };
      if(styleConfig && styleConfig.displayMode && styleConfig.displayMode!=''){
        config.displayMode = styleConfig.displayMode;
      }

      new this.offAmazonPayments.Widgets.AddressBook(config).bind(element.nativeElement.id);
    }
  }
  loadConsentWidget(element:ElementRef,billingAgreementId : string,styleConfig?:StyleConfig,onReadyCallback?:Function,onConsent?:Function,onErrorCallback?:Function){
    if(this.sdkLoaded){
      let config:any = {
        sellerId: this.sellerId,
        agreementType: 'BillingAgreement',
        amazonBillingAgreementId: billingAgreementId,
        onReady: function(billingAgreementConsentStatus) {
          console.log(billingAgreementConsentStatus);
            onReadyCallback?onReadyCallback(billingAgreementConsentStatus):null;
        },
        design: styleConfig.responsive ? { designMode: 'responsive' } : {
  size: { width: styleConfig.width, height: styleConfig.height }
},
        onConsent: function(billingAgreementConsentStatus) {
          console.log("Consent provided");
          console.log(billingAgreementConsentStatus)
          // Display your custom complete purchase button
          onConsent?onConsent(billingAgreementConsentStatus):null;
        },
        onError: function(error) {
          // Write your custom error handling
          onErrorCallback?onErrorCallback(error):null;
        }
      };
      if(styleConfig && styleConfig.displayMode && styleConfig.displayMode!=''){
        config.displayMode = styleConfig.displayMode;
      }

      new this.offAmazonPayments.Widgets.Consent(config).bind(element.nativeElement.id);
    }
  }
}
