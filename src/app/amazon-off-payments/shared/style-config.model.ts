export class StyleConfig {
  width: string = '400px';
  height: string = '400px';
  displayMode : string;
  responsive :boolean = false;
}
