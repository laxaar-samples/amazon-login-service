import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import {OffPaymentsService} from './shared/off-payments.service';
import {StyleConfig} from './shared/style-config.model';
declare var amazonConfig;
@Component({
  moduleId: module.id,
  selector: 'amazon-off-payments',
  templateUrl: 'amazon-off-payments.component.html',
  styleUrls: ['amazon-off-payments.component.css'],
  providers : [OffPaymentsService]
})
export class AmazonOffPaymentsComponent implements OnInit {
  private waiting : boolean;
  private sdkLoaded :boolean;
  private error:string;
  private sellerId : string
  private widgetLoaded :boolean = false;
  private billingAgreementId :string;
  @ViewChild("walletWidget") walletWidget:ElementRef;
  @ViewChild("consentWidget") consentWidget:ElementRef;

  constructor(private offPaymentsService:OffPaymentsService) {
    this.waiting = true;
    this.sellerId = amazonConfig.sellerId;
    this.offPaymentsService.loadSdk(amazonConfig.sellerId,amazonConfig.sandboxMode,()=>{
      this.sdkLoaded = true;
      this.waiting = false;
      //this.loadWalletWidget();
    },(error)=>{
      console.log(error);
      this.error = error;
      this.sdkLoaded = false;
      this.waiting = false;
    });
   }

  ngOnInit() {
  }

  loadWalletWidget(){

    let onReady = (billingAgreement)=>{
      console.log("Wallet Widget Called Ready");
      this.widgetLoaded = true;
      console.log(this);
      this.billingAgreementId = billingAgreement.getAmazonBillingAgreementId();
    };
    let onPaymentSelect = (orderReference)=>{
      console.log("On Payment Selected");
      console.log(orderReference);
      this.loadConsentWidget();
    };
    let onError = (error)=>{
      alert("Error while loading wallet Widget");
    };
    let styleConfig = new StyleConfig();
    this.offPaymentsService.loadWalletWidget(this.walletWidget,this.billingAgreementId,styleConfig,onReady,onPaymentSelect,onError);
  }
  loadConsentWidget(){
    let styleConfig = new StyleConfig();
    let onReady = (billingAgreementConsentStatus)=>{
      console.log("Consent Widget Called Ready");
      console.log(billingAgreementConsentStatus);
    };
    let onConsent = (billingAgreementConsentStatus)=>{
      console.log("On Consent Called");
      console.log(billingAgreementConsentStatus);
      alert("Consent Provided");
    };
    let onError = (error)=>{
      console.log(error.getErrorMessage());
      alert("Error while loading Consent Widget--"+error.getErrorMessage());
    };
    this.offPaymentsService.loadConsentWidget(this.consentWidget,this.billingAgreementId,styleConfig,onReady,onConsent,onError);
  }

}
