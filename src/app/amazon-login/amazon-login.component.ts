import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import {AmazonAuthenticationService} from './shared/amazon-authentication.service';
import {Options} from './shared/options.model';
declare var amazonConfig;
@Component({
  moduleId: module.id,
  selector: 'amazon-login',
  templateUrl: 'amazon-login.component.html',
  styleUrls: ['amazon-login.component.css'],
  /*This is included in Parent App Component. Uncomment it if not included there*/
  //providers : [AmazonAuthenticationService]
})
export class AmazonLoginComponent  {
  private clientId : string = amazonConfig.clientId;//'amzn1.application-oa2-client.f53afc8ab5c8453a94c05d770c9b1b3c';
  private sdkLoaded :boolean;
  private waiting : boolean;
  private error:string;
  private accessToken : string;
  private loggedIn:boolean;
  private profileDetails : any;

  constructor(private amazonAuthenticationService : AmazonAuthenticationService) {
    this.waiting = true;
    this.amazonAuthenticationService.loadSdk(this.clientId,amazonConfig.sandboxMode,()=>{
      this.sdkLoaded = true;
      this.waiting = false;
    },(error)=>{
      console.log(error);
      this.error = error;
      this.sdkLoaded = false;
      this.waiting = false;
    });
  }

  login(){
    if(this.sdkLoaded){
      let options = new Options();
      options.scope='profile payments:widget'
      this.amazonAuthenticationService.authorize(options,(response)=>{
        if(response.status=='complete' && response.access_token){
          this.accessToken = response.access_token;
          this.amazonAuthenticationService.setAccessToken(response.access_token);
          this.loggedIn = true;
          this.retrieveProfileDetails();
        }
      });
    }
  }
  retrieveProfileDetails(){
    this.amazonAuthenticationService.retrieveProfile((response)=>{
      console.log(response);
      this.profileDetails = JSON.stringify(response.profile);
    },this.accessToken);
  }
  logout(){
    if(this.sdkLoaded){
      this.amazonAuthenticationService.logout();
      this.loggedIn = false;
      this.profileDetails = null;
    }
  }
}
