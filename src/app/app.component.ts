import { Component } from '@angular/core';
import {AmazonLoginComponent} from './amazon-login';
import {AmazonAuthenticationService} from './amazon-login/shared/amazon-authentication.service';
import {AmazonOffPaymentsComponent} from './amazon-off-payments';
@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  providers : [AmazonAuthenticationService],
  directives : [AmazonLoginComponent,AmazonOffPaymentsComponent]
})
export class AppComponent {
  constructor(private amazonAuthenticationService:AmazonAuthenticationService){

  }
  isLoggedIn(){
    return this.amazonAuthenticationService.isLoggedIn();
  }
}
